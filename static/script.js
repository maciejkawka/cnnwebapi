function setFormenu() {
    var forMenu = document.getElementById("formenu");
    let heightdivFormenu = forMenu.offsetHeight;
    let screenHeight = window.innerHeight;
    if (heightdivFormenu >= screenHeight) {
        forMenu.style.top = '0px';
    } else {
        var tmp = (screenHeight - heightdivFormenu) / 2;
        forMenu.style.top = tmp.toString() + 'px';
    }
}

setFormenu();

window.onresize = setFormenu;

usedInception = false;
usedMobilnet = false;
usedMyModel = false;

const sections = document.getElementsByClassName('mainElement');
const navLi = document.querySelectorAll('#formenu ul li');
var serverImageResponse;
var inputValue;
var usedInception = false;
var usedMobilnet = false;
var usedMyModel = false;

window.addEventListener('scroll', () => {
    let current = '';

    Array.prototype.forEach.call(sections, (s) => {
        const sectionOffset = s.offsetTop;
        if (pageYOffset >= (sectionOffset - 100)) {
            current = s.getAttribute('id');
        }
    })
    navLi.forEach(li => {
        li.classList.remove('active');
        if (li.classList.contains(current)) {
            li.classList.add('active');
        }
    })
})

function sendHttpRequest(txt, destination) {
    var path = 'http://127.0.0.1:8000/' + txt;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", path, true);
    xhr.onload = function () {
        if (xhr.status == 200) {
            destination.innerHTML = xhr.response;
        }
    }
    xhr.send();

}

function openOption(element) {
    var tmpElement = document.getElementById(element);
    var headerElement = tmpElement.children[0];
    var optionElement = headerElement.children[0];
    var contentElement = tmpElement.children[1];
    if (optionElement.innerHTML == "▼") {
        optionElement.innerHTML = "&#9650";
        contentElement.style.display = "block";
        contentElement.innerHTML = '<div class="loader"></div>';
        sendHttpRequest(element, contentElement);

    } else {
        optionElement.innerHTML = "&#9660";
        contentElement.innerHTML = " ";
        contentElement.style.display = "none";
    }
}

function dragOverHandler(ev) {
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
}

function dropHandler(ev) {
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

    if (ev.dataTransfer.items[0].kind === 'file') {
        const file = ev.dataTransfer.items[0].getAsFile();
        if (file.type.match('^image/jpeg') || file.type.match('^image/png')) {
            var forInfotoImage = document.getElementById("forInfotoImage");
            var tmpText = 'File name: ' + file.name + '</br>';
            tmpText += 'File type: ' + file.type + '</br>';
            var tmpsize = file.size / 1048576;
            if (tmpsize > 1) {
                tmpsize *= 100;
                tmpsize = Math.round(tmpsize)
                tmpsize /= 100;
                tmpText += 'File size: ' + tmpsize + 'MB</br>';
            } else {
                tmpsize = file.size / 1024;
                tmpsize *= 100;
                tmpsize = Math.round(tmpsize)
                tmpsize /= 100;
                tmpText += 'File size: ' + tmpsize + 'kB</br>';
            }
            forInfotoImage.innerHTML = tmpText
            forInfotoImage.style.display = "block";
            var fileRader = new FileReader();
            fileRader.readAsDataURL(file);
            fileRader.onloadend = function (ev) {
                sendFileToServer(ev, file);
            }
        }
    }
}

function createForm(where) {
    var p = document.createElement('p');
    p.innerHTML = 'Please select model';
    where.append(p);
    var divzew = document.createElement('div');

    var divchoise1 = document.createElement('div');
    var inputMobilnet = document.createElement('input');
    inputMobilnet.type = 'radio';
    inputMobilnet.id = 'contactChoiceMobilnet';
    inputMobilnet.addEventListener('click', function () {
        radioClick('Mobilnet');
    });
    var labelMobilenet = document.createElement('label');
    labelMobilenet.innerHTML = 'Mobilnet';
    divchoise1.append(inputMobilnet);
    divchoise1.append(labelMobilenet);
    divzew.append(divchoise1);
    //ddddddddddddddddddd
    var divchoise2 = document.createElement('div');
    var inputInception = document.createElement('input');
    inputInception.type = 'radio';
    inputInception.id = 'contactChoiceInception';
    inputInception.addEventListener('click', function () {
        radioClick('Inception');
    });
    var labelInception = document.createElement('label');
    labelInception.innerHTML = 'Inception';
    divchoise2.append(inputInception);
    divchoise2.append(labelInception);
    divzew.append(divchoise2);
    var divchoise3 = document.createElement('div');
    var inputMyModel = document.createElement('input');
    inputMyModel.type = 'radio';
    inputMyModel.id = 'contactChoiceMyModel';
    inputMyModel.addEventListener('click', function () {
        radioClick('MyModel');
    });
    var labelMyModel = document.createElement('label');
    labelMyModel.innerHTML = 'MyModel';
    divchoise3.append(inputMyModel);
    divchoise3.append(labelMyModel);
    divzew.append(divchoise3);

    where.append(divzew);
    var br = document.createElement('br');
    where.append(br);
    var divforButtonSend = document.createElement('div');
    divforButtonSend.classList.add('divforButtonSend');
    var button = document.createElement('button');
    button.type = 'button';
    button.addEventListener('click', function () {
        runModel();
    });
    button.innerHTML = 'Test';
    button.classList.add('sendButton');
    divforButtonSend.append(button);
    where.append(divforButtonSend);

}

function sendFileToServer(evt, file) {
    var tmpTxt = "<img width=\"380\" height=\"380\" src='";
    tmpTxt += evt.target.result + "'/>";
    document.getElementById("forFile").innerHTML = tmpTxt;
    var formData = new FormData;
    formData.append("file", file)
    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'http://127.0.0.1:8000/doTheTest', true);
    xhr.onload = function () {
        if (xhr.status == 200) {
            serverImageResponse = xhr.response;
            console.log(serverImageResponse);
            var forSendForm = document.getElementById("forSendForm");
            createForm(forSendForm);
            forSendForm.style.display = 'block';
        } else {
            alert("sendFileToServer: " + xhr.response);
        }
    }
    xhr.send(formData);
}

function radioClick(str) {

    if (str === 'Mobilnet') {
        usedMobilnet = true;
        document.getElementById('contactChoiceInception').checked = false;
        document.getElementById('contactChoiceMyModel').checked = false;
    }
    if (str === 'Inception') {
        usedInception = true;
        document.getElementById('contactChoiceMobilnet').checked = false;
        document.getElementById('contactChoiceMyModel').checked = false;
    }
    if (str === 'MyModel') {
        usedMyModel = true;
        document.getElementById('contactChoiceInception').checked = false;
        document.getElementById('contactChoiceMobilnet').checked = false;
    }
    inputValue = str;
}

function createResultView(where, listObjects, model, status) {
    if (status) {
        var forAllContener = document.createElement('div');
        forAllContener.classList.add('forAllContener');
        var forTitleinResultContner = document.createElement('div');
        forTitleinResultContner.classList.add('forTitleinResultContner');
        forTitleinResultContner.innerHTML = model + ' result is:';
        forAllContener.append(forTitleinResultContner);
        var forResultinContener = document.createElement('div');
        forResultinContener.classList.add('forResultinContener');

        for (var i = 0; i < listObjects.length; i++) {
            var resultElement = document.createElement('div');
            resultElement.classList.add('resultElement');
            var tmpObject = listObjects[i];
            resultElement.innerHTML = tmpObject.label
                + ' : ' + tmpObject.probability + ' %';
            forResultinContener.append(resultElement);
        }
        where.innerHTML = '';
        forAllContener.append(forResultinContener);
        where.append(forAllContener);
        createNotUsedModelButton();
    }
}

function createNotUsedModelButton() {
    var servrespone = JSON.parse(serverImageResponse);
    if (!usedInception) {
        var forIn = document.getElementById('forIn');
        var smallcontener = document.createElement('div');
        var button = document.createElement('div');
        button.classList.add('secoundModel');
        button.innerHTML = "Run Inception Model"
        smallcontener.append(button);
        forIn.append(smallcontener);
        usedInception = true;
        button.addEventListener('click', function () {
            var path = 'http://127.0.0.1:8000/testImage?image=' +
                servrespone.fileServerName +
                '&model=Inception';
            var xhr = new XMLHttpRequest();
            forIn.innerHTML = '</br>'+ "<div class=\"loader\"></div>";
            xhr.open("POST", path, true);
            xhr.onload = function () {
                if (xhr.status == 200) {
                    const objectResponse = JSON.parse(xhr.response);
                    if (objectResponse.status) {
                        createResultView(forIn, objectResponse.result, objectResponse.modelto, true);
                    }
                }
            }
            xhr.send();
            console.log('send Inception');
        })

    }
    if (!usedMobilnet) {
        var forMibil = document.getElementById('forMobil');
        var smallcontener = document.createElement('div');
        var button = document.createElement('div');
        button.classList.add('secoundModel');
        button.innerHTML = "Run Mobilnet Model"
        smallcontener.append(button);
        forMibil.append(smallcontener);
        usedMobilnet = true;
        button.addEventListener('click', function () {
            var path = 'http://127.0.0.1:8000/testImage?image=' +
                servrespone.fileServerName +
                '&model=Mobilnet';
            var xhr = new XMLHttpRequest();
            forMibil.innerHTML = '</br>'+ "<div class=\"loader\"></div>";
            //var sendForm = document.getElementById("forSendForm");
            //sendForm.innerHTML = "<div class=\"loader\"></div>";
            xhr.open("POST", path, true);
            xhr.onload = function () {
                if (xhr.status == 200) {
                    const objectResponse = JSON.parse(xhr.response);
                    if (objectResponse.status) {
                        createResultView(forMibil, objectResponse.result, objectResponse.modelto, true);
                    }
                }
            }
            xhr.send();
            console.log('send Mobilnet');
        })

    }
    if (!usedMyModel) {
        var forMym = document.getElementById('forMy');
        var smallcontener = document.createElement('div');
        var button = document.createElement('div');
        button.classList.add('secoundModel');
        button.innerHTML = "Run MyModel"
        smallcontener.append(button);
        forMym.append(smallcontener);
        usedMyModel = true;
        button.addEventListener('click', function () {
            var path = 'http://127.0.0.1:8000/testImage?image=' +
                servrespone.fileServerName +
                '&model=MyModel';
            var xhr = new XMLHttpRequest();
            forMym.innerHTML = '</br>'+ "<div class=\"loader\"></div>";
            xhr.open("POST", path, true);
            xhr.onload = function () {
                if (xhr.status == 200) {
                    const objectResponse = JSON.parse(xhr.response);
                    if (objectResponse.status) {
                        createResultView(forMym, objectResponse.result, objectResponse.modelto, true);
                    }
                }
            }
            xhr.send();
            console.log('send MyModel');
        })

    }
}

function runModel() {
    var servrespone = JSON.parse(serverImageResponse);
    if (inputValue.length > 3 && servrespone.fileServerName.length > 3) {
        console.log("ifClick")
        var path = 'http://127.0.0.1:8000/testImage?image=' +
            servrespone.fileServerName +
            '&model=' + inputValue;
        var xhr = new XMLHttpRequest();
        var sendForm = document.getElementById("forSendForm");
        sendForm.innerHTML = "<div class=\"loader\"></div>";
        xhr.open("POST", path, true);
        xhr.onload = function () {
            if (xhr.status == 200) {
                const objectResponse = JSON.parse(xhr.response);
                if (objectResponse.status) {
                    createResultView(sendForm, objectResponse.result, objectResponse.modelto, true);
                }
            }
        }
        xhr.send();
    }
}

function sendEmail(target, title, msg) {
    var path = 'http://127.0.0.1:8000/Contact/sendemail?target='
        + target + '&title='
        + title + '&msg='
        + msg;
    var xhr = new XMLHttpRequest();
    xhr.open('POST', path, true);
    xhr.onload = function () {
        if (xhr.status == 200) {
            alert('poszlo');
        }
    }
    xhr.send();
}

function validAndSendEmail() {

    function validateEmail(email) {
        return email.match(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
    }

    let targetEmail = document.getElementById('inputemail').value
    console.log(validateEmail(targetEmail));
    if (validateEmail(targetEmail)) {
        var flagTitle = null;
        var flagText = null;
        let titleEmail = document.getElementById('inputtitle').value
        let textEmail = document.getElementById('inputtext').value

        flagTitle = true ? titleEmail.toString().length > 2 : null
        if (!flagTitle) {
            document.getElementById('labelinputtitle').innerHTML = 'write a title please';
        } else {
            document.getElementById('labelinputtitle').innerHTML = '';
        }

        flagText = true ? textEmail.toString().length > 2 : null
        if (!flagText) {
            document.getElementById('labelinputtext').innerHTML = 'write something kind';
        } else {
            document.getElementById('labelinputtext').innerHTML = '';
        }

        if (flagTitle && flagText) {
            sendEmail(targetEmail, titleEmail, textEmail);
        }

    } else {
        document.getElementById('labelinputemail').innerHTML = 'email invalid';
    }

}

