## ABOUT 

A simple application built on FastApi using CNN neural network models

### Models: 

- mobilenet
- inception
- myModel

### You can:

- check the model information
- upload the photo to the server
- choose a model and make an analysis


### Technologies:

- Python
- FastApi
- CRUD operations
- JavaScript

### Authors:

- Maciej Kawka
