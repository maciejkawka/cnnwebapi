from fastapi import FastAPI, Request, UploadFile, File
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/")
async def root(request: Request):
    template = Jinja2Templates(directory="htmlFiles")
    return template.TemplateResponse("index.html", {"request": request, "title": "iScanner"})


import staticElements as staticElement


@app.post("/Test")
async def aboutTest():
    return HTMLResponse(content=staticElement.tmpTest, status_code=200)

@app.post("/Models")
async def aboutTest():
    return HTMLResponse(content=staticElement.tmpModels, status_code=200)

@app.post("/AndroidAP")
async def aboutTest():
    return HTMLResponse(content=staticElement.tmpAndroidAP, status_code=200)

@app.post("/Contact")
async def aboutTest():
    return HTMLResponse(content=staticElement.tmpContact, status_code=200)

import smtplib
@app.post("/Contact/sendemail")
async def aboutTest(target: str, title: str, msg: str):
    print('start')
    smtp_server = smtplib.SMTP_SSL('smtp.gmail.com', 465)

    sender = 'iscannermessage@gmail.com'
    password = 'iscannerMessage321'
    smtp_server.login(sender,password)
    smtp_server.sendmail(target,sender,title + ' ' +msg )
    smtp_server.quit()

    return {"stat": "ok"}


import shutil
import time


@app.post("/doTheTest")
async def getFile(file: UploadFile):
    filesevrverloc = 'files/' + str(round(time.time() * 1000)) + file.filename
    with open(filesevrverloc, 'wb') as content:
        shutil.copyfileobj(file.file, content)
    return {"filname": file.filename,
            "fileServerName": filesevrverloc,
            "fileType": file.content_type}


import runModel


@app.post("/testImage")
async def testImage(image: str, model: str):
    if (len(image) != 0 and len(model) != 0):
        result = ""
        if (model == "Mobilnet"):
            result = runModel.runMobilnetModel(image)
            print("Mobilnet")
        elif (model == "Inception"):
            result = runModel.runInceprionModel(image)
            print("Inception")
        else:
            print("else")
            result = runModel.runMyModel(image)
        return {"status": True, "modelto": model, "link": image, "result": result}
    else:
        return {"status": "nieokk"}
