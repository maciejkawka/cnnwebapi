import tensorflow as tf

import time

class ModelAnalysisResults:
    def __init__(self, probability, label):
        self.probability = probability
        self.label = label


#  1x150x150x3
MYMODEL = "models/model.tflite"
#  1x224x224x3
MOBILNET = "models/mobilenet_v1_1.0_224_quant.tflite"
#  1x299x299x3
INCEPTION = "models/inception_v4_quant_1_default_1.tflite"


def getDataAboutModelFromInterpreter(intepreter):
    input_details = intepreter.get_input_details()
    output_details = intepreter.get_output_details()
    print(input_details)
    print("== Input details ==")
    print("name:", input_details[0]['name'])
    print("shape:", input_details[0]['shape'])
    print("type:", input_details[0]['dtype'])

    print("\n== Output details ==")
    print("name:", output_details[0]['name'])
    print("shape:", output_details[0]['shape'])
    print("type:", output_details[0]['dtype'])


def getInputShapeFromInterpreter(intepreter):
    input_details = intepreter.get_input_details()
    shape = input_details[0]['shape_signature']
    type = input_details[0]['dtype']
    return (shape[1], shape[2])


def getListLabelsFromFile(fileName):
    lista = []
    file = open(fileName, 'r')
    for line in file:
        line = line.replace('\n', '')
        lista.append(line)
    file.close()
    return lista


def expPredictAndReturnSUM(predicts):
    import math
    sum = 0
    lista = []
    for pred in predicts:
        sum += math.exp(pred)
        lista.append(math.exp(pred))
    return lista, sum


def resultFromModel(imagePath, tflite_interpreter, type):
    dsize = getInputShapeFromInterpreter(tflite_interpreter)
    tflite_interpreter.allocate_tensors()

    import cv2
    src = cv2.imread(imagePath)
    input_data = cv2.resize(src, dsize, interpolation=cv2.INTER_CUBIC)

    import numpy as np
    if (type == 'float32'):
        input_data = input_data.astype(np.float32)
        input_data /= 255.

    input_tensor = np.array(np.expand_dims(input_data, 0))
    input_index = tflite_interpreter.get_input_details()[0]["index"]
    tflite_interpreter.set_tensor(input_index, input_tensor)
    tflite_interpreter.invoke()
    output_details = tflite_interpreter.get_output_details()
    output_data = tflite_interpreter.get_tensor(output_details[0]['index'])
    return np.squeeze(output_data)


def prepareListObjects(lista, expPP, suma):
    listObjects = []
    iter = 0
    lessThen5pp = 0
    sumLessThen5pp = 0
    for probability in expPP:
        tmpProbability = (probability / suma) * 100
        tmpProbability = round(tmpProbability, 2)
        if (tmpProbability >= 5.0):
            listObjects.append(ModelAnalysisResults(tmpProbability, lista[iter]))
        else:
            if (tmpProbability >= 0.01):
                lessThen5pp += 1
            sumLessThen5pp += tmpProbability
        iter += 1

    listObjects.sort(key=lambda x: x.probability, reverse=True)
    if (sumLessThen5pp > 0):
        listObjects.append(ModelAnalysisResults(sumLessThen5pp,
                                                "The number greater than 0 but less than 5 is "
                                                + str(lessThen5pp)))
    return listObjects


def runMobilnetModel(imagePath):
    tflite_interpreter = tf.lite.Interpreter(model_path=MOBILNET)
    preds = resultFromModel(imagePath, tflite_interpreter, type="uint")
    listProbability, sum = expPredictAndReturnSUM(preds)
    listLabels = getListLabelsFromFile('models/labels.txt')
    listObjects = prepareListObjects(listLabels, listProbability, sum)
    return listObjects


def runInceprionModel(imagePath):
    tflite_interpreter = tf.lite.Interpreter(model_path=INCEPTION)
    preds = resultFromModel(imagePath, tflite_interpreter, type="uint")
    listProbability, sum = expPredictAndReturnSUM(preds)
    listLabels = getListLabelsFromFile('models/labels.txt')
    listObjects = prepareListObjects(listLabels, listProbability, sum)
    return listObjects


def runMyModel(imagePath):
    tflite_interpreter = tf.lite.Interpreter(model_path=MYMODEL)
    preds = resultFromModel(imagePath, tflite_interpreter, type='float32')
    listProbability, sum = expPredictAndReturnSUM(preds)
    listLabels = getListLabelsFromFile('models/myLabels.txt')
    listObjects = prepareListObjects(listLabels, listProbability, sum)
    return listObjects


