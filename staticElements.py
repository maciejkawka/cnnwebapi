tmpTest = ''' 
<div id="forFile" ondrop="dropHandler(event);" ondragover="dragOverHandler(event);">
    <div id="tmpText">Drop a file here</div>
</div>
<div id="forOptions">
    <div id="forInfotoImage"></div>
    <div id="forSendForm"></div>
</div>
</div id="forRest">
    <div id="forMy"></div>
    <div id="forIn"></div>
    <div id="forMobil"></div>
</div>
'''


tmpModels = '''
<div style="width : 80%; margin-top:20px; margin-left:auto; margin-right:auto">Here you will find basic information about the models I use in the application</div>
<div style="width : 80%; margin-top:20px; margin-left:auto; margin-right:auto"><b>1.Inception V4</b></div>
<div style="width : 80%; margin-left:auto; margin-right:auto">Takes input values of 299x299px, recognizes among 1,000 classes</div>
<div style="width : 80%; margin-bottom: 20px; margin-left:auto; margin-right:auto">Network structure below:</div>
<img class="imgCenter" src="static/inceptionModel.jpg" alt="Inception Model" height="220" width="70%">
<div style="width : 80%; margin-top:20px; margin-left:auto; margin-right:auto"><b>2.MobileNet V1</b></div>
<div style="width : 80%; margin-left:auto; margin-right:auto">Takes input values of 224x224px, recognizes among 1,000 classes</div>
<div style="width : 80%; margin-left:auto; margin-right:auto">In addition to the usual convolution layers, it also has depthwise separable convolutions, 
the task of which is to speed up the work of the neural network</div>
<div style="width : 80%; margin-bottom: 20px; margin-left:auto; margin-right:auto">Network structure below:</div>
<img class="imgCenter" src="static/MobilenetModel.jpg" alt="Mobilenet Model" height="640" width="571">

<div style="width : 80%; margin-top:20px; margin-left:auto; margin-right:auto"><b>3. MyModel</b></div>
<div style="width : 80%; margin-left:auto; margin-right:auto">Takes input values of 150x150px, recognizes among 5 classes</div>
<div style="width : 80%; margin-bottom: 20px; margin-left:auto; margin-right:auto">Network structure:</div>
<div style="width : 80%; margin-bottom: 20px; margin-left:auto; margin-right:auto">
    <ul>
        <li>Conv with 8 filters</li>
        <li>Conv with 16 filters</li>
        <li>Conv with 32 filters</li>
        <li>Conv with 64 filters</li>
        <li>FC 64</li>
        <li>FC 64</li>
        <li>FC 5</li>
    </ul>
</div>
'''

tmpContact = '''
<div style="width:70%; margin-right: auto;margin-left: auto;"><br><br>
If you need contact regarding the application or mobile application, please contact me via the form below<br><br>
<label id="labelinputemail" for="text"></label>
<input type="email" id="inputemail" name="fname" placeholder="Your e-mail">
<label id="labelinputtitle" for="text"></label>
<input type="text" id="inputtitle" name="fname" placeholder="Title..">
<label id="labelinputtext" for="text"></label>
<input type="text" id="inputtext" name="fname" placeholder=":)">
<div style="width:100%; text-align: center;">
<button type="button" onClick="validAndSendEmail()" class="sendButton">&#9993 Send e-mail</button>
</div>
</div>
'''

tmpAndroidAP = '''
<div style="width:70%; margin-right: auto;margin-left: auto;"><br><br>
You can use a mobile version of this app, where You can use this the same models <br><br>

You can do photo or pick from device: </br></br></br>
<div class="forAndroidAPPimages">
<img src="static/frontApp.jpg" class="appimages" alt="img">
<img src="static/frontAppWithIMG.jpg" class="appimages" alt="img">
</div></br></br></br>
Test one of the model:</br></br></br>
<div class="forAndroidAPPimages">
<img src="static/fIMGwithObject.jpg" class="appimages" alt="img"></br>
</div></br></br></br>
And compere with the rest models:</br></br></br>
<div class="forAndroidAPPimages">
<img src="static/compareObject.jpg" class="appimages" alt="img"></br>
</div></br></br></br>
'''

'''
<div id="forSendForm">
        <p>Please select model:</p>
        <div>
            <div>
                <input type="radio" id="contactChoice1" name="contact" value="Mobilnet" onclick="radioClick(this.value)">
                <label for="contactChoice1">Mobilnet</label>
            </div>
            <div>
                <input type="radio" id="contactChoice2" name="contact" value="Inception" onclick="radioClick(this.value)">
                <label for="contactChoice2">Inception</label>
            </div>
            <div>
                <input type="radio" id="contactChoice3" name="contact" value="MyModel" onclick="radioClick(this.value)">
                <label for="contactChoice3">MyModel</label>
            </div>
        </div>
        </br>
        <div class ="divforButtonSend">
        <button type="button" onClick="runModel();" class="sendButton">Test</button>
        </div>
    </div>
'''